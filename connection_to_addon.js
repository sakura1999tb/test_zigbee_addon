const mqtt = require("mqtt");
const client = mqtt.connect("mqtt://localhost");

const data_service_to_zigbee_addon = {
  src: "connection",
  audit: {},
  query: {
    action: "update",
    actor: "connection",
    device_id: "jadsfakjdk_123123124_1231231248134092143-12",
    data: {
      command: {
        action: "open",
      },
    },
  },
};

client.on("connect", function () {
  client.publish(
    "service/zigbee/request",
    JSON.stringify(data_service_to_zigbee_addon)
  );
});
