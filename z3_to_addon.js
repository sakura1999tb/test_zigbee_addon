const mqtt = require("mqtt");
const client = mqtt.connect("mqtt://localhost");

const data_z3_to_zigbee_addon = {
  clusterId: "0x12",
  attributeId: "0x0000",
  attributeBuffer: "0x12",
  attributeDataType: "12",
  deviceEndpoint: {
    eui64: "0x123AFE",
    endpoint: 123,
  },
};

client.on("connect", function () {
  client.publish(
    "gw/eui64_id/zclresponse",
    JSON.stringify(data_z3_to_zigbee_addon)
  );
});
